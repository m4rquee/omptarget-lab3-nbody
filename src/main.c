#include <omp.h>
#include <gtk/gtk.h>

static void print_stuff(GtkWidget *widget, gpointer data) {
#pragma omp parallel for
  for (int i = 0; i < 10; i++)
    g_print("%d ", i);
  g_print("\n");
}

static void activate(GtkApplication *app, gpointer user_data) {
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *button_box;

  window = gtk_application_window_new(app);
  gtk_window_set_title(GTK_WINDOW(window), "NBody simulation");
  gtk_window_set_default_size(GTK_WINDOW(window), 500, 500);

  button_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add(GTK_CONTAINER(window), button_box);

  button = gtk_button_new_with_label("run");
  g_signal_connect(button, "clicked", G_CALLBACK(print_stuff), NULL);
  g_signal_connect(button, "clicked", G_CALLBACK(gtk_widget_destroy), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), button);

  gtk_widget_show_all(window);
}

int main(int argc, char **argv) {
  GtkApplication *app;
  int status;

  app = gtk_application_new("ic.nbody", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  status = g_application_run(G_APPLICATION(app), argc, argv);
  g_object_unref(app);

  return status;
}
